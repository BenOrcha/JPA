package test.com.univ;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.univ.Person;
import com.univ.Rent;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	EntityManagerFactory emf = Persistence.createEntityManagerFactory("manager1");
		EntityManager entityManager = emf.createEntityManager();
		
		EntityTransaction tx = entityManager.getTransaction();
		
    	try{
    		
			tx.begin();
			
			Person p = new Person();
			p.setName("Tintin");
			
			Rent rent = new Rent();
					
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date beginDate = dateFormat.parse("28/9/2017");
			rent.setBenginRent(beginDate);
			
			Date endDate = dateFormat.parse("30/9/2017");
			rent.setEndRent(endDate);
			
			rent.setPerson(p);
			List<Rent> rents = p.getRents();
			rents.add(rent);
			p.setRents(rents);
			
			entityManager.persist(p);
				
			tx.commit();			
			
			long id = p.getId();
			
			Person p1 = entityManager.find(Person.class, id);
			System.out.println(p1);
			
			Query query = entityManager.createQuery("select p from Person p where p.name = 'Tintin'");
			List persons = query.getResultList();
			
			System.out.println(persons.get(0));
			
		}catch(Exception e){
			tx.rollback();
		}
		
	}
}
