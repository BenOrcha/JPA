package com.univ;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Rent {
	
	long key;
	Date benginRent;
	Date endRent;
	Person person;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getKey() {
		return key;
	}

	public void setKey(long key) {
		this.key = key;
	}

	@Temporal(TemporalType.DATE)
	public Date getBenginRent() {
		return benginRent;
	}

	public void setBenginRent(Date benginRent) {
		this.benginRent = benginRent;
	}

	@Temporal(TemporalType.DATE)
	public Date getEndRent() {
		return endRent;
	}

	public void setEndRent(Date endRent) {
		this.endRent = endRent;
	}

	@ManyToOne
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "Rent [key=" + key + ", benginRent=" + benginRent + ", endRent="
				+ endRent + "]";
	}


	
	
	
	

}
